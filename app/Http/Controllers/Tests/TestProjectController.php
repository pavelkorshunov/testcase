<?php

namespace App\Http\Controllers\Tests;


use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TestProjectController extends Controller
{
    public function index()
    {
        return view('tests.create');
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:projects|min:3',
            'project_url' => 'required|unique:projects|url'
        ]);

        $current_user_id = Auth::user()->id;

        Project::create(['name' => $request->name, 'project_url' => $request->project_url, 'user_id' => $current_user_id]);

        return redirect()->back()->with('success', 'Your project create');
    }
}