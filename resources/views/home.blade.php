@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">Add project</div>
                <div class="card-body">
                    <a class="btn btn-success" href="{{ route('create', [], false) }}">Add new project</a>
                </div>
            </div>

            <div class="card">
                <div class="card-header">Projects</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="project">
                        <div class="project-name">Divier</div>
                        <a class="btn btn-primary" href="#">Select</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
