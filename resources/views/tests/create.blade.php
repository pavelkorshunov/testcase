@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        @if (session()->has('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <form action="{{ route('create', [], false) }}" method="post">
                            <div class="form-group">
                                <label for="project-name">Project name</label>
                                <input type="text" class="form-control" id="project-name" name="name" placeholder="Enter project name">
                            </div>
                            <div class="form-group">
                                <label for="project-url">Project URL</label>
                                <input type="text" class="form-control" id="project-url" name="project_url" placeholder="Enter project url">
                            </div>
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection